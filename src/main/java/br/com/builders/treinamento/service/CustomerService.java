package br.com.builders.treinamento.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.converter.CustomerToCustomerResponse;
import br.com.builders.treinamento.dto.converter.CustomerRequestToCustomer;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.repository.CustomerRepository;

@Component
@Lazy
public class CustomerService {
	
	@Inject
    private CustomerRepository repository;
	
	@Inject
    CustomerRequestToCustomer customerRequestToCustomer;
	
	@Inject	
    CustomerToCustomerResponse customerToCustomerResponse;
	
	public CustomerResponse save(CustomerRequest customerRequest) {
        Customer customer = repository.save(customerRequestToCustomer.convert(customerRequest));
        return customerToCustomerResponse.convert(customer);
    }
	
	public void delete(String customerId) throws NotFoundException {
        findById(customerId);
        repository.delete(customerId);
    }
	
	public CustomerResponse findById(String customerId) throws NotFoundException {
        Customer customer = repository.findOne(customerId);
        if (customer == null) {
            throw new NotFoundException("The customer is not found");
        }
        return customerToCustomerResponse.convert(customer);
    }
	
	public List<CustomerResponse> findAll() {
		return customerToCustomerResponse.convert(repository.findAll());
    }

    public CustomerResponse patchUpdate(CustomerRequest customerRequest, String customerId) throws NotFoundException {
        CustomerResponse customerResponse = findById(customerId);

        Customer.CustomerBuilder builder = Customer.builder().id(customerResponse.getId());

        if (!StringUtils.isEmpty(customerRequest.getLogin()))
            builder.login(customerRequest.getLogin());
        else
            builder.login(customerResponse.getLogin());

        if (!StringUtils.isEmpty(customerRequest.getName()))
            builder.name(customerRequest.getName());
        else
            builder.name(customerResponse.getName());

        if (!StringUtils.isEmpty(customerRequest.getBaseUrl()))
            builder.baseUrl(customerRequest.getBaseUrl());
        else
            builder.baseUrl(customerResponse.getBaseUrl());

        if (!StringUtils.isEmpty(customerRequest.getCrmId()))
            builder.crmId(customerRequest.getCrmId());
        else
            builder.crmId(customerResponse.getCrmId());

        Customer customer = builder.build();
        repository.save(customer);

        return customerToCustomerResponse.convert(customer);
    }
    
    public CustomerResponse update(CustomerRequest customerRequest, String customerId) throws NotFoundException {
        CustomerResponse customerResponse = findById(customerId);

        Customer customer = customerRequestToCustomer.convert(customerRequest);
        customer.setId(customerResponse.getId());

        return customerToCustomerResponse.convert(repository.save(customer));
    }
}
