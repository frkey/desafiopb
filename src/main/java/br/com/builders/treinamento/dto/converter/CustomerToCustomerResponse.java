package br.com.builders.treinamento.dto.converter;

import org.springframework.stereotype.Component;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.response.CustomerResponse;

@Component
public class CustomerToCustomerResponse implements GenericConverter<Customer, CustomerResponse> {
	
	@Override
	public CustomerResponse apply(Customer input) {
		return CustomerResponse.builder()
                .id(input.getId())
                .name(input.getName())
                .crmId(input.getCrmId())
                .login(input.getLogin())
                .baseUrl(input.getBaseUrl())
                .build();
	}
}
