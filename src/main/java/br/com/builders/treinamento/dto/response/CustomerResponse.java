package br.com.builders.treinamento.dto.response;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.URL;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.builders.treinamento.exception.ErrorCodes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties({"uuid"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "CustomerResponse")
public class CustomerResponse {
	
	/**
	 * Internal Customer ID, uniquely identifying this customer in the world.
	 * @return id
	 **/
	@ApiModelProperty(example = "553fa88c-4511-445c-b33a-ddff58d76886", value = "Internal Customer ID, uniquely identifying this customer in the world. ")
	@JsonProperty("id")
	@NotNull(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
    private String id;
	
	/**
	 * Customer ID in the CRM.
	 * @return crmId
	 **/
	@ApiModelProperty(example = "C645235", value = "Customer ID in the CRM. ")
	@JsonProperty("crmId")
	private String crmId;
	
	/**
	 * Base URL of the customer container.
	 * @return baseUrl
	 **/
	@ApiModelProperty(example = "http://www.platformbuilders.com.br", value = "Base URL of the customer container. ")
	@JsonProperty("baseUrl")
	@URL
	private String baseUrl;
	
	/**
	 * Customer name 
	 * @return name
	 **/
	@ApiModelProperty(example = "Platform Builders", value = "Customer name ")
	@JsonProperty("name")
	private String name;

	/**
	 * Admin login
	 * @return login
	 **/
	@ApiModelProperty(example = "contato@platformbuilders.com.br", value = "Admin login ")
	@JsonProperty("login")
	@Email
	private String login;
}
