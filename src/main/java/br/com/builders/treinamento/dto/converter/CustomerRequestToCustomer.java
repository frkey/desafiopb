package br.com.builders.treinamento.dto.converter;

import org.springframework.stereotype.Component;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;

@Component
public class CustomerRequestToCustomer implements GenericConverter<CustomerRequest, Customer> {

	@Override
	public Customer apply(CustomerRequest input) {
		return Customer.builder()
                .crmId(input.getCrmId())
                .baseUrl(input.getBaseUrl())
                .name(input.getName())
                .login(input.getLogin())
                .build();
	}
}
