package br.com.builders.treinamento.dto.converter;

import org.springframework.stereotype.Component;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;

@Component
public class CustomerToCustomerRequest implements GenericConverter<Customer, CustomerRequest>{
	
	@Override
	public CustomerRequest apply(Customer input) {
		return CustomerRequest.builder()
                .name(input.getName())
                .crmId(input.getCrmId())
                .login(input.getLogin())
                .baseUrl(input.getBaseUrl())
                .build();
	}
}
