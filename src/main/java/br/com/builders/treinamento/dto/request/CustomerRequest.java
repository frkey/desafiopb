/**
 * DTO CustomerRequest
 * 
 * @author Felipe Carvalho
 *
 */
package br.com.builders.treinamento.dto.request;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.URL;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"uuid"})
@ApiModel(value = "CustomerRequest")
public class CustomerRequest {
	
	/**
	 * Customer ID in the CRM.
	 * @return crmId
	 **/
	@ApiModelProperty(example = "C645235", value = "Customer ID in the CRM.")
	private String crmId;
	
	/**
	 * Base URL of the customer container.
	 * @return baseUrl
	 **/
	@ApiModelProperty(example = "http://www.platformbuilders.com.br", value = "Base URL of the customer container.")
	@URL
	private String baseUrl;
	
	/**
	 * Customer name 
	 * @return name
	 **/
	@ApiModelProperty(example = "Platform Builders", value = "Customer name")
	private String name;

	/**
	 * Admin login
	 * @return login
	 **/
	@ApiModelProperty(example = "contato@platformbuilders.com.br", value = "Admin login")
	@Email
	private String login;
}
