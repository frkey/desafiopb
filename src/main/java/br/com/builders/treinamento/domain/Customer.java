
/**
 * Entity Customer
 * 
 * @author Felipe Carvalho
 *
 */
package br.com.builders.treinamento.domain;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import br.com.builders.treinamento.exception.ErrorCodes;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(of = "id")
@Document(collection = "felipe_carvalho_customer")
public class Customer {
	
	@Id
	@NotBlank(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
	private String id;
	
	@NotBlank
	private String crmId;
	
	@URL
	@NotBlank
	private String baseUrl;
	
	@NotBlank
	private String name;
	
	@Email
    @Indexed(unique = true)
	@NotBlank
	private String login;
}
