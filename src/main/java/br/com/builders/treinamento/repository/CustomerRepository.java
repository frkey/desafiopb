package br.com.builders.treinamento.repository;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.builders.treinamento.domain.Customer;

@RepositoryRestResource(collectionResourceRel = "customer", path = "customer")
@Lazy
public interface CustomerRepository
	extends MongoRepository<Customer, String> {

}
