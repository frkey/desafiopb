package br.com.builders.treinamento.resources;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "the customers API")
public class CustomerResource {
	
	@Inject
	private CustomerService customerService;

	@ApiOperation(value = "Creates new customer", nickname = "createCustomer", notes = "TODO ", tags={ "system", })
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Item created"),
        @ApiResponse(code = 400, message = "Invalid input, object invalid"),
        @ApiResponse(code = 409, message = "An existing item(login) already exists")})
    @RequestMapping(value = "/customers", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON, method = RequestMethod.POST)
    public CustomerResponse createCustomer(
    		@ApiParam(value = "Customer to create") @Valid @RequestBody CustomerRequest customerRequest) {
		 return customerService.save(customerRequest);	
	}

    @ApiOperation(value = "Deletes a customer", nickname = "deleteCustomer", notes = "TODO ", tags={ "system", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Item deleted"),
        @ApiResponse(code = 404, message = "The customer is not found") })
    @RequestMapping(value = "/customers/{customerId}", method = RequestMethod.DELETE)
    public void deleteCustomer(
    		@ApiParam(value = "ID of customer that needs to be deleted", required=true) @PathVariable("customerId") String customerId) throws NotFoundException {
    	customerService.delete(customerId);    	
    }

    @ApiOperation(value = "List all data about a customer", nickname = "getCustomer", notes = "TODO ", response = CustomerResponse.class, tags={ "customers", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "A single customer", response = CustomerResponse.class),
        @ApiResponse(code = 404, message = "Customer with this ID not found") })
    @RequestMapping(value = "/customers/{customerId}",
        produces = MediaType.APPLICATION_JSON, 
        method = RequestMethod.GET)
    public CustomerResponse getCustomer(
    		@ApiParam(value = "ID of customer that needs to be fetched",required=true) @PathVariable("customerId") String customerId) throws NotFoundException {
    			return customerService.findById(customerId);    	
    }

    @ApiOperation(value = "List all customers / containers (with essential data, enough for displaying a table)", nickname = "listCustomers", notes = "TODO ", response = CustomerResponse.class, responseContainer = "List", tags={ "customers", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Search results matching criteria", response = CustomerResponse.class, responseContainer = "List") })
    @RequestMapping(value = "/customers",
        produces = MediaType.APPLICATION_JSON, 
        method = RequestMethod.GET)
    public List<CustomerResponse> listCustomers() {
    	return customerService.findAll();
    	
    }

    @ApiOperation(value = "Modifies a customer", nickname = "modifyCustomer", notes = "TODO ", tags={ "system", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Item modified"),
        @ApiResponse(code = 400, message = "Invalid input, object invalid"),
        @ApiResponse(code = 404, message = "The customer (or any of supplied IDs) is not found") })
    @RequestMapping(value = "/customers/{customerId}",
        produces = MediaType.APPLICATION_JSON, 
        consumes = MediaType.APPLICATION_JSON,
        method = RequestMethod.PATCH)
    public void modifyCustomer(
    		@ApiParam(value = "ID of customer that needs to be modified",required=true) @PathVariable("customerId") String customerId,
    		@ApiParam(value = "Customer data with one or more fields filled")  @Valid @RequestBody CustomerRequest customerRequest) throws NotFoundException {
    	customerService.patchUpdate(customerRequest, customerId);    	
    }


    @ApiOperation(value = "Replaces a customer", nickname = "replaceCustomer", notes = "TODO ", tags={ "system", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Item replaced"),
        @ApiResponse(code = 400, message = "Invalid input, object invalid"),
        @ApiResponse(code = 404, message = "The customer (or any of supplied IDs) is not found") })
    @RequestMapping(value = "/customers/{customerId}",
        produces = MediaType.APPLICATION_JSON, 
        consumes = MediaType.APPLICATION_JSON,
        method = RequestMethod.PUT)
    public void replaceCustomer(
    		@ApiParam(value = "ID of customer that needs to be replaced", required=true) @PathVariable("customerId") String customerId,
    		@ApiParam(value = "Customer to replace") @Valid @RequestBody CustomerRequest customerRequest) throws NotFoundException {
    	customerService.update(customerRequest, customerId);
    	
    }
}
