package br.com.builders.treinamento.customer;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.builders.treinamento.dto.converter.CustomerRequestToCustomer;
import br.com.builders.treinamento.dto.converter.CustomerToCustomerResponse;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import br.com.builders.treinamento.exception.GlobalExceptionHandler;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.resources.CustomerResource;
import br.com.builders.treinamento.service.CustomerService;

@RunWith(MockitoJUnitRunner.class)
public class CustomerResourceTest {
	
    private static final String ID_CUSTOMER = "91685498-7ce7-47d8-b14b-cfa9588fc797";
	private static final String NAME = "Felipe Carvalho";
	private static final String BASE_URL = "http://platformbuilders.io";
	private static final String CRM_ID = "0000/SP";
	private static final String LOGIN =  "contato@platformbuilders.io";
	
	@Spy
    CustomerRequestToCustomer customerRequestConverter;
	
	@Spy	
    CustomerToCustomerResponse customerToCustomerResponse;

    @Mock
    private CustomerService customerService;
    
    @InjectMocks
    private CustomerResource customerResource;

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Before
    public void init() {
    	MockitoAnnotations.initMocks(this);
    	objectMapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(customerResource).setControllerAdvice(new GlobalExceptionHandler(Mockito.mock(MessageSource.class)))
                .build();
    }
    
    @Test
    public void testSave() throws Exception {
        when(customerService.save(customerRequestTest)).thenReturn(customerResponseTest);

        mockMvc.perform(MockMvcRequestBuilders.post("/customers").contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(customerResponseTest))
                ).andExpect(MockMvcResultMatchers.status().isOk());
    }
    
    @Test
    public void testSaveExistingLogin() throws Exception {
        when(customerService.save(customerRequestTest))
                .thenThrow(new DataIntegrityViolationException("An existing item(login) already exists"));

        mockMvc.perform(MockMvcRequestBuilders.post("/customers").contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(customerResponseTest))
                ).andExpect(MockMvcResultMatchers.status().isConflict());
    }
    
    @Test
    public void testDelete() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/customers/{customerId}", ID_CUSTOMER))
            .andExpect(MockMvcResultMatchers.status().isOk());
    }
    
    @Test
    public void testFindById() throws Exception {
        when(customerService.findById(ID_CUSTOMER)).thenReturn(customerResponseTest);

        mockMvc.perform(MockMvcRequestBuilders.get("/customers/{customerId}", ID_CUSTOMER))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", equalTo(ID_CUSTOMER)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
    
    @Test
    public void testFindByIdWhenNotFound() throws Exception {
        when(customerService.findById(ID_CUSTOMER)).thenThrow(new NotFoundException("Customer with this ID not found"));

        mockMvc.perform(MockMvcRequestBuilders.get("/customers/{customerId}", ID_CUSTOMER))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
    
    @Test
    public void testFindAll() throws Exception {
        when(customerService.findAll()).thenReturn(Arrays.asList(customerResponseTest));

        mockMvc.perform(MockMvcRequestBuilders.get("/customers"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", equalTo(NAME)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].baseUrl", equalTo(BASE_URL)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].crmId", equalTo(CRM_ID)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].login", equalTo(LOGIN)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testFindAllWhenResultIsEmpty() throws Exception {
        when(customerService.findAll()).thenReturn(new ArrayList<>());

        mockMvc.perform(MockMvcRequestBuilders.get("/customers")).andExpect(MockMvcResultMatchers.jsonPath("$", empty()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
    
    @Test
    public void testPatchUpdate() throws Exception {
        CustomerRequest request = CustomerRequest.builder()
                .name("Joselito")
                .baseUrl("https://bit.ly/2w2r1vX")
                .build();

        mockMvc.perform(MockMvcRequestBuilders.patch("/customers/{customerId}", ID_CUSTOMER)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(request))
                ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testPatchUpdateWhenNotFound() throws Exception {
        doThrow(new NotFoundException("Customer with this ID not found"))
                .when(customerService).patchUpdate(Matchers.any(CustomerRequest.class), Matchers.eq(ID_CUSTOMER));

        mockMvc.perform(MockMvcRequestBuilders
                .patch("/customers/{customerId}", ID_CUSTOMER)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(customerRequestTest))
                ).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testUpdate() throws Exception {

        CustomerRequest request = customerRequestTest;

        mockMvc.perform(MockMvcRequestBuilders.put("/customers/{customerId}", ID_CUSTOMER)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(request))
                ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testUpdateWhenNotFound() throws Exception {
        doThrow(new NotFoundException("Customer with this ID not found"))
                .when(customerService).update(Matchers.any(CustomerRequest.class), Matchers.eq(ID_CUSTOMER));

        mockMvc.perform(MockMvcRequestBuilders
                .put("/customers/{customerId}", ID_CUSTOMER)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(customerRequestTest))
                ).andExpect(MockMvcResultMatchers.status().isNotFound());
    }
    
    private final CustomerRequest customerRequestTest = CustomerRequest.builder().crmId(CRM_ID).baseUrl(BASE_URL).name(NAME).login(LOGIN).build();
    private final CustomerResponse customerResponseTest = CustomerResponse.builder().id(ID_CUSTOMER).crmId(CRM_ID).baseUrl(BASE_URL).name(NAME).login(LOGIN).build();
}