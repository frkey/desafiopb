package br.com.builders.treinamento.customer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.converter.CustomerRequestToCustomer;
import br.com.builders.treinamento.dto.converter.CustomerToCustomerResponse;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.service.CustomerService;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {
	
	private static final String ID_CUSTOMER = "91685498-7ce7-47d8-b14b-cfa9588fc797";
	private static final String NAME = "Joselito";
	private static final String BASE_URL = "http://platformbuilders.io";
	private static final String CRM_ID = "1111/MG";
	private static final String LOGIN =  "contato@platformbuilders.io";
	
	@Spy
    CustomerRequestToCustomer customerRequestToCustomer;
	
	@Spy	
    CustomerToCustomerResponse customerToCustomerResponse;

    @Mock
    private CustomerRepository repository;    

    @InjectMocks
    private CustomerService service;
    
    @Test
    public void testSave() {
        Mockito.when(repository.save(any(Customer.class))).thenReturn(customerTest);

        CustomerResponse customerResponse = service.save(customerRequestTest);

        assertEquals(customerTest.getId(), customerResponse.getId());
        assertEquals(customerTest.getCrmId(), customerResponse.getCrmId());
        assertEquals(customerTest.getBaseUrl(), customerResponse.getBaseUrl());
        assertEquals(customerTest.getName(), customerResponse.getName());        
        assertEquals(customerTest.getLogin(), customerResponse.getLogin());
    }    

    @Test
    public void findAll() {

        Mockito.when(repository.findAll()).thenReturn(Arrays.asList(customerTest));

        List<CustomerResponse> customers = service.findAll();
        
        assertEquals(customers.get(0).getCrmId(), CRM_ID);
        assertEquals(customers.get(0).getId(), ID_CUSTOMER);

    }   

    @Test
    public void testPatchUpdate() throws NotFoundException {

        Mockito.when(repository.findOne(anyString())).thenReturn(customerTest);

        CustomerRequest customerRequest = CustomerRequest.builder()
                .name("Felipe Carvalho")
                .build();
        CustomerResponse customerResponse = service.patchUpdate(customerRequest, customerTest.getId());

        assertNotEquals(customerTest.getName(), customerResponse.getName());
        assertNotNull(customerResponse.getBaseUrl());
        assertNotNull(customerResponse.getCrmId());
    }
    
    private final Customer customerTest = Customer.builder().id(ID_CUSTOMER).crmId(CRM_ID).baseUrl(BASE_URL).name(NAME).login(LOGIN).build();
    private final CustomerRequest customerRequestTest = CustomerRequest.builder().crmId(CRM_ID).baseUrl(BASE_URL).name(NAME).login(LOGIN).build();

}